import os
from google.cloud import bigquery
import json
import boto3
import base64
import math

ssm = boto3.client('ssm', region_name='ap-southeast-1')

file='/tmp/test-gcp-account.json'
# with open(file, 'w') as filetowrite:
#     filetowrite.write(r'''{}'''.format(ssm.get_parameter(Name=os.environ["BQ_CREDENTIALS"], WithDecryption=True)['Parameter']['Value']))
with open(file, 'w', encoding="utf-8") as filetowrite:
    filetowrite.write(r'''{
  "type": "service_account",
  "project_id": "production-268602",
  "private_key_id": "3d65437695e02c1c506e03d3bbdb8ce2f76170b6",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDHzhr0lpN7wMop\n7gxi9QoUZ/f1bP7H05X2q9YNiJI56J9qrDWHS4jdstknkecYjKgNw9mA1tuUGzoE\n9DNuPHfV5Z6cjpFCCsKHoUkG3x5POKYgnS21/JUra+8T/7Q/X8dquMl/Dpfnl6/Z\nfGpfn1kAHDnF9RXrbuiaoOcDDKC9oNnW2A49I+tEvkHrmTi2H5xMo+Ky8jVC4vem\ncEsRvkCRNXHDll0kGpqVWKZuofUdhBcVpqGc3BpkmCuqO6NsMX7lScThmhbQ1Cs9\nwH5JQEvijZclq/xHarokfGd6QFeZ87AP07sZmCg6Jl1nsp0Ayv8JGzt8r4ttxhAT\n0wJvc82jAgMBAAECggEADb2b1vRAAkO443dOfMOzRMH9qHAteVqQtp0MpHOe8sKP\nrAtZxwsnx29URcdf6C1MJV4xqA2enindLJgKrWsVJq2a4dNgI/WPeTpbRlCzyejw\na8/DY/+R1qGemHZ+U9lnYmyA1u8O/UtOOZu3yQmxWjOWuYloT3+aGYA1Qt1sdH3r\nOENUv01QFWzqPzgSnbdZbQBv8CR7uIUrfM+Nady9jxDMB6GQH6X6GvQ+VKhkLGPu\ndQ3LGKXIJxUlZ7UW3h/oH7EVGdEqLVEMoIcIXqeR6Mt4ATrZS4UAFK+ea2xPdAg3\nD0IKB57zlTqbBe/ogFUMxsvU6v3Npoxn9SAGY37TWQKBgQDtgcVat1NYNwNZXhkP\nOy9r6QvZwkBsLB9FGRoqnGnJ4TW9iJsbDteXhicP3pskT97LUoDWuXlipaPMDpUZ\ngKSpAqoCklZlMMsgGmYYn55tuaIgDyIAtPULLtwukoFL2ayyTE0b5mQW6x/oiyzk\ne/PVHThNh4xpLwEJWPpIeIpB/QKBgQDXXNLM0d4Dpavugll9gA1gXZSv5p3pv8CS\nsCcj18GqyNt1uboCHRgyxpUzOV1jpSQVWSTC98ivoZJdZq8yUlCra9epm2s6M/zk\nMmyxVrT6QWEcR/a51T2HyPDff7P6EREGin4V4Cl9uwyTndGGSygZLIw2GrK+8BkB\natDNTc4QHwKBgCHWYECpZ7VkuRRtZJKX8wyjui1/EGCY9KeKiQDTIftCG77yDG8R\ncUPEOCJ5q47D1lEPYw20hQe545k55qIogs0VdRVCtg5aokXjnPIbI54sWV3J6iVB\norvCJFp6QvOEgqcwWbgE3fOp8fTWh0HSKw4MyK27BxUhPjuPsrDKg6AdAoGAZ2yT\na3tp5FR+kgcVVPjW861dR3A369aDc66WzVbDEfJtukacNoGkalYFttjYqY7YGkx6\nJAt83CN6t9IlVsRDK1+Z7UDPKN9olmpE2pFqTjhxm3RP34SxjPEKBDs+wlvJ0ltW\nG8lQA6p9arLCuXyEYDqNhzohFyMUzyJt7fpseD8CgYBOKZOvtFR7eSDx0/kgNS/Y\nucJ06kIVVuz9kemomwRyQIP6ebkXQhPO4KALUCLr8x0HHk4NeVeeXVhme1ihFSQG\nd/ZdaBESD27eDsZHMDCmc9uH7xMB952qrVs+XnHwTpB2nVfS3/nlz0KclJ86FtbZ\n5GQzDwHqisTSGgunKVw3gQ==\n-----END PRIVATE KEY-----\n",
  "client_email": "edmondtempserviceaccount@production-268602.iam.gserviceaccount.com",
  "client_id": "104597691220277759115",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/edmondtempserviceaccount%40production-268602.iam.gserviceaccount.com"
}''')


os.environ["GOOGLE_APPLICATION_CREDENTIALS"]= "/tmp/test-gcp-account.json"

client = bigquery.Client()

def clean_field(row):
    if row is None:
        return None
    elif type(row) == str:
        return int(row)
    elif type(row) == int or type(row) == float:
        if math.isnan(row):
            return None
        else:
            return row
    else:
        return None

def lambda_handler(event, context):

    data_rows = event

    # TODO(developer): Set table_id to the ID of table to append to.
    # table_id = "profound-surge-259005.GoogleStudio.stg-engagements-analytics"
    table_id = "{}.{}.{}".format(os.environ['BQ_PROJECT_NAME'], os.environ['BQ_DATASET_NAME'], os.environ['BQ_TABLE_ID'])

    rows_to_insert = []

    for doc in data_rows:
        data = {
            'app_user_id': clean_field(doc['app_user_id']),
            'transaction_date': None if doc['transaction_date'] == "" else doc['transaction_date'],
            'activity': None if doc['activity'] == "" else doc['activity'],
            'mobile_number': None if doc['mobile_number'] == "" else doc['mobile_number'],
            'country_id': clean_field(doc['country_id']),
            'country_code': None if doc['country_code'] == "" else doc['country_code'],
            'operating_system': None if doc['operating_system'] == "" else doc['operating_system'],
            # 'referral_code': None if doc['referral_code'] == "" else doc['referral_code'],
            'country_id': clean_field(doc['country_id']),
            # 'referrer_app_user_id': clean_field(doc['referrer_app_user_id'])
        }

        rows_to_insert.append(data)

    print(rows_to_insert)

    errors = client.insert_rows_json(
        table_id, rows_to_insert, row_ids=[None] * len(rows_to_insert)
    )  # Make an API request.
    if errors == []:
        print("New rows have been added.")
        return {
            'statusCode': 200,
            'body': json.dumps('New rows have been added.')
        }
    else:
        print("Encountered errors while inserting rows: {}".format(errors))
        return {
            'statusCode': 400,
            'body': json.dumps('Encountered errors while inserting rows.')
        }
